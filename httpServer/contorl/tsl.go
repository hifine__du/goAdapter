package contorl

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"goAdapter/device"
	"goAdapter/httpServer/model"
	"goAdapter/setting"
	"goAdapter/utils"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

func ApiAddDeviceTSL(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	tslInfo := &struct {
		TSLName    string `json:"TSLName"`    // 名称
		TSLExplain string `json:"TSLExplain"` // 描述
	}{}

	err := json.Unmarshal(bodyBuf[:n], tslInfo)
	if err != nil {
		setting.ZAPS.Error("tslInfo格式化错误")
		context.JSON(http.StatusOK, model.Response{
			Code:    "1",
			Message: "tslInfo格式化错误",
			Data:    "",
		})
		return
	}

	setting.ZAPS.Debugf("tslInfo %v", tslInfo)

	tslParam := device.NewDeviceTSL(tslInfo.TSLName, tslInfo.TSLExplain)
	_, err = device.DeviceTSLAdd(tslParam)
	if err != nil {
		setting.ZAPS.Errorf("DeviceTSLAdd err %v", err)
		aParam.Code = "1"
		aParam.Message = "DeviceTSLAdd err"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	aParam.Code = "0"
	aParam.Message = ""
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiDeleteDeviceTSL(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	tslInfo := &struct {
		TSLName string `json:"TSLName"` // 名称
	}{}

	err := json.Unmarshal(bodyBuf[:n], tslInfo)
	if err != nil {
		fmt.Println("tslInfo json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	setting.ZAPS.Debugf("tslInfo %v", tslInfo)

	_, err = device.DeviceTSLDelete(tslInfo.TSLName)
	if err != nil {
		setting.ZAPS.Errorf("DeviceTSLDelete err %v", err)
		aParam.Code = "1"
		aParam.Message = err.Error()
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	aParam.Code = "0"
	aParam.Message = ""
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiModifyDeviceTSL(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	tslInfo := &struct {
		TSLName    string `json:"TSLName"`    // 名称
		TSLExplain string `json:"TSLExplain"` // 描述
	}{}

	err := json.Unmarshal(bodyBuf[:n], tslInfo)
	if err != nil {
		setting.ZAPS.Errorf("tslInfo json unMarshall err %v", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	setting.ZAPS.Debugf("tslInfo %v", tslInfo)

	for _, v := range device.DeviceTSLMap {
		if v.Name == tslInfo.TSLName {
			_, err := device.DeviceTSLModifyExplain(tslInfo.TSLName, tslInfo.TSLExplain)
			if err != nil {
				setting.ZAPS.Errorf("DeviceTSLModify err %v", err)
				aParam.Code = "1"
				aParam.Message = "DeviceTSLModify err"
				sJson, _ := json.Marshal(aParam)
				context.String(http.StatusOK, string(sJson))
				return
			}
			aParam.Code = "0"
			aParam.Message = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	aParam.Code = "1"
	aParam.Message = "tSLName is not exist"
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiGetDeviceTSL(context *gin.Context) {

	type TSLInfoTemplate struct {
		Name    string
		Explain string
		Plugin  string
	}

	aParam := struct {
		Code    string            `json:"Code"`
		Message string            `json:"Message"`
		Data    []TSLInfoTemplate `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    make([]TSLInfoTemplate, 0),
	}

	for _, v := range device.DeviceTSLMap {
		tslInfo := TSLInfoTemplate{
			Name:    v.Name,
			Explain: v.Explain,
			Plugin:  v.Plugin,
		}
		aParam.Data = append(aParam.Data, tslInfo)
	}

	aParam.Code = "0"
	aParam.Message = ""

	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiGetDeviceTSLContents(context *gin.Context) {

	type DeviceTSLPropertyIntUintParamTempate struct {
		Min         string `json:"Min"`         //最小
		Max         string `json:"Max"`         //最大
		MinMaxAlarm bool   `json:"MinMaxAlarm"` //范围报警
		Step        string `json:"Step"`        //步长
		StepAlarm   bool   `json:"StepAlarm"`   //阶跃报警
		Unit        string `json:"Unit"`        //单位
	}

	type DeviceTSLPropertyDoubleParamTempate struct {
		Min         string `json:"Min"`         //最小
		Max         string `json:"Max"`         //最大
		MinMaxAlarm bool   `json:"MinMaxAlarm"` //范围报警
		Step        string `json:"Step"`        //步长
		StepAlarm   bool   `json:"StepAlarm"`   //阶跃报警
		Decimals    string `json:"Decimals"`    //小数位数
		Unit        string `json:"Unit"`        //单位
	}

	type DeviceTSLPropertyStringParamTempate struct {
		DataLength      string `json:"DataLength,omitempty"`      //字符串长度
		DataLengthAlarm bool   `json:"DataLengthAlarm,omitempty"` //字符长度报警
	}

	type DeviceTSLPropertyTemplate struct {
		Name       string      `json:"Name"`       //属性名称，只可以是字母和数字的组合
		Explain    string      `json:"Explain"`    //属性解释
		AccessMode int         `json:"AccessMode"` //读写属性
		Type       int         `json:"Type"`       //类型 uint32 int32 double string
		Params     interface{} `json:"Params"`
	}

	type DeviceTSLTemplate struct {
		Properties []DeviceTSLPropertyTemplate       `json:"Properties"` //属性
		Services   []device.DeviceTSLServiceTempalte `json:"Services"`   //服务
	}

	tslInfo := DeviceTSLTemplate{}

	aParam := struct {
		Code    string            `json:"Code"`
		Message string            `json:"Message"`
		Data    DeviceTSLTemplate `json:"Data"`
	}{
		Code:    "1",
		Message: "",
	}

	tslName := context.Query("TSLName")
	for _, v := range device.DeviceTSLMap {
		if v.Name == tslName {

			tslInfo.Services = v.Services

			tslInfo.Properties = make([]DeviceTSLPropertyTemplate, 0)
			property := DeviceTSLPropertyTemplate{}
			for _, p := range v.Properties {
				property.Name = p.Name
				property.Explain = p.Explain
				property.AccessMode = p.AccessMode
				property.Type = p.Type
				switch p.Type {
				case device.PropertyTypeUInt32:
					fallthrough
				case device.PropertyTypeInt32:
					{
						intUintPropertyParam := DeviceTSLPropertyIntUintParamTempate{
							Min:         p.Params.Min,
							Max:         p.Params.Max,
							MinMaxAlarm: p.Params.MinMaxAlarm,
							Step:        p.Params.Step,
							StepAlarm:   p.Params.StepAlarm,
							Unit:        p.Params.Unit,
						}
						property.Params = intUintPropertyParam
					}
				case device.PropertyTypeDouble:
					{
						doublePropertyParam := DeviceTSLPropertyDoubleParamTempate{
							Min:         p.Params.Min,
							Max:         p.Params.Max,
							MinMaxAlarm: p.Params.MinMaxAlarm,
							Step:        p.Params.Step,
							StepAlarm:   p.Params.StepAlarm,
							Unit:        p.Params.Unit,
							Decimals:    p.Params.Decimals,
						}
						property.Params = doublePropertyParam
					}
				case device.PropertyTypeString:
					{
						stringPropertyParam := DeviceTSLPropertyStringParamTempate{
							DataLength:      p.Params.DataLength,
							DataLengthAlarm: p.Params.DataLengthAlarm,
						}
						property.Params = stringPropertyParam
					}
				}
				tslInfo.Properties = append(tslInfo.Properties, property)
			}

			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = tslInfo
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))

			return
		}
	}

	aParam.Code = "1"
	aParam.Message = "tslName is not exist"

	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiImportDeviceTSLContents(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	// 获取物模型名称
	TSLName := context.PostForm("TSLName")

	index := -1
	for k, v := range device.DeviceTSLMap {
		if v.Name == TSLName {
			index = k
		}
	}
	if index == -1 {
		setting.ZAPS.Errorf("TSLName is not exist")
		aParam.Code = "1"
		aParam.Message = "TSLName is not exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	// 获取文件头
	file, err := context.FormFile("FileName")
	if err != nil {
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	fileName := exeCurDir + "/selfpara/" + file.Filename

	//保存文件到服务器本地
	err = utils.FileCreate("/selfpara/" + file.Filename)
	if err != nil {
		setting.ZAPS.Errorf("创建文件错误 %v", err)
	}
	if err := context.SaveUploadedFile(file, fileName); err != nil {
		aParam.Code = "1"
		aParam.Message = "保存文件错误"

		setting.ZAPS.Errorf("保存文件错误 %v", err)
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	defer os.Remove(fileName)

	result := setting.LoadCsvCfg(fileName, 1, 2, 1) //标题在第2行，从第3行取数据，第2列取数据
	if result == nil {
		aParam.Code = "1"
		aParam.Message = "Load csv File err"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}
	setting.ZAPS.Debugf("records %v", result.Records)

	for _, record := range result.Records {
		//tslName := record.GetString("TSLName")
		//index := -1
		//for k, v := range device.DeviceTSLMap {
		//	if v.Name == tslName {
		//		index = k
		//	}
		//}
		//if index == -1 {
		//	setting.ZAPS.Errorf("TSLName is not exist")
		//	aParam.Code = "1"
		//	aParam.Message = "TSLName is not exist"
		//	sJson, _ := json.Marshal(aParam)
		//	context.String(http.StatusOK, string(sJson))
		//	return
		//}

		if record.GetString("ContentType") == "property" {
			propertyParam := device.DeviceTSLPropertyParamTempate{
				Min:         record.GetString("Min"),
				Max:         record.GetString("Max"),
				MinMaxAlarm: record.GetBool("MinMaxAlarm"),
				Step:        record.GetString("Step"),
				StepAlarm:   record.GetBool("StepAlarm"),
				Decimals:    record.GetString("Decimals"),
				Unit:        record.GetString("Unit"),
				DataLength:  record.GetString("DataLength"),
			}

			property := device.DeviceTSLPropertyTemplate{
				Name:       record.GetString("Name"),
				Explain:    record.GetString("Explain"),
				AccessMode: record.GetInt("AccessMode"),
				Type:       record.GetInt("Type"),
				Params:     propertyParam,
				Value:      make([]device.DeviceTSLPropertyValueTemplate, 0),
			}
			_, err = device.DeviceTSLMap[index].DeviceTSLPropertiesAdd(property)
			if err != nil {
				setting.ZAPS.Errorf("DeviceTSLPropertiesAdd err %v", err)
				aParam.Code = "1"
				aParam.Message = err.Error()
				sJson, _ := json.Marshal(aParam)
				context.String(http.StatusOK, string(sJson))
				return
			}
		} else if record.GetString("ContentType") == "service" {
			service := device.DeviceTSLServiceTempalte{
				Name:    record.GetString("Name"),
				Explain: record.GetString("Explain"),
				Params:  make(map[string]interface{}),
			}
			callType := record.GetString("CallType")
			if callType == "synchronous" {
				service.CallType = 0
			} else if callType == "asynchronous" {
				service.CallType = 1
			}

			_, err = device.DeviceTSLMap[index].DeviceTSLServicesAdd(service)
			if err != nil {
				setting.ZAPS.Errorf("DeviceTSLServicesAdd err %v", err)
				aParam.Code = "1"
				aParam.Message = err.Error()
				sJson, _ := json.Marshal(aParam)
				context.String(http.StatusOK, string(sJson))
				return
			}
		}
	}

	aParam.Code = "0"
	aParam.Message = ""
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiExportDeviceTSLContents(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	tslName := context.Query("TSLName")
	index := -1
	for k, v := range device.DeviceTSLMap {
		if v.Name == tslName {
			index = k
		}
	}

	if index == -1 {
		aParam.Code = "1"
		aParam.Message = "tslName is not Exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	//创建文件
	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	fileName := exeCurDir + "/selfpara/" + tslName + ".csv"

	fs, err := os.Create(fileName)
	if err != nil {
		setting.ZAPS.Errorf("creat tsl.csv err,%v", err)
		return
	}

	defer os.Remove(fileName)
	defer fs.Close()
	// 写入UTF-8 BOM
	//_, err = fs.WriteString("\xEF\xBB\xBF")

	//创建一个新的写入文件流
	csvFile := csv.NewWriter(fs)
	csvRecords := [][]string{
		{"模型名称", "功能类型", "功能名称", "标识符", "读写类型", "数据类型",
			"单位", "最小值", "最大值", "范围报警", "步长", "步长报警", "小数位", "字符串长度", "字符串长度报警", "服务调用方式"},
		{"TSLName", "ContentType", "Name", "Explain", "AccessMode", "Type",
			"Unit", "Min", "Max", "MinMaxAlarm", "Step", "StepAlarm", "Decimals", "DataLength", "DataLengthAlarm", "CallType"},
	}

	for _, v := range device.DeviceTSLMap[index].Properties {
		record := make([]string, 0)
		record = append(record, device.DeviceTSLMap[index].Name)
		record = append(record, "property")
		record = append(record, v.Name)
		record = append(record, v.Explain)
		record = append(record, strconv.Itoa(v.AccessMode))
		record = append(record, strconv.Itoa(v.Type))
		record = append(record, v.Params.Unit)
		record = append(record, v.Params.Min)
		record = append(record, v.Params.Max)
		str := ""
		if v.Params.MinMaxAlarm == true {
			str = "true"
		} else {
			str = "false"
		}
		record = append(record, str)
		record = append(record, v.Params.Step)
		if v.Params.StepAlarm == true {
			str = "true"
		} else {
			str = "false"
		}
		record = append(record, str)
		record = append(record, v.Params.Decimals)
		record = append(record, v.Params.DataLength)
		if v.Params.DataLengthAlarm == true {
			str = "true"
		} else {
			str = "false"
		}
		record = append(record, str)
		record = append(record, "-")
		csvRecords = append(csvRecords, record)
	}

	for _, v := range device.DeviceTSLMap[index].Services {
		record := make([]string, 0)
		record = append(record, device.DeviceTSLMap[index].Name)
		record = append(record, "service")
		record = append(record, v.Name)
		record = append(record, v.Explain)
		//空
		for i := 0; i < 11; i++ {
			record = append(record, "-")
		}
		if v.CallType == 0 {
			record = append(record, "synchronous")
		} else {
			record = append(record, "asynchronous")
		}
		csvRecords = append(csvRecords, record)
	}

	err = csvFile.WriteAll(csvRecords)
	if err != nil {
		aParam.Code = "1"
		aParam.Message = "csv write records err"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}
	csvFile.Flush()

	//返回文件流
	context.Writer.Header().Add("Content-Disposition",
		fmt.Sprintf("attachment;filename=%s", filepath.Base(fileName)))
	context.File(fileName) //返回文件路径，自动调用http.ServeFile方法

	return
}

func ApiExportDeviceTSLContentsTemplate(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	//创建文件
	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	fileName := exeCurDir + "/selfpara/tslPropertyTemplate.csv"

	fs, err := os.Create(fileName)
	if err != nil {
		setting.ZAPS.Errorf("creat tslPropertyTemplate.csv err %v", err)
		return
	}
	defer os.Remove(fileName)
	defer fs.Close()
	// 写入UTF-8 BOM
	//_, err = fs.WriteString("\xEF\xBB\xBF")

	//创建一个新的写入文件流
	csvFile := csv.NewWriter(fs)

	csvRecords := [][]string{
		{"模型名称", "功能类型", "功能名称", "标识符", "读写类型", "数据类型",
			"单位", "最小值", "最大值", "范围报警", "步长", "步长报警", "小数位", "字符串长度", "字符串长度报警", "服务调用方式"},
		{"TSLName", "ContentType", "Name", "Explain", "AccessMode", "Type",
			"Unit", "Min", "Max", "MinMaxAlarm", "Step", "StepAlarm", "Decimals", "DataLength", "DataLengthAlarm", "CallType"},
	}

	err = csvFile.WriteAll(csvRecords)
	if err != nil {
		aParam.Code = "1"
		aParam.Message = "csv write records err"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}
	csvFile.Flush()

	//返回文件流
	context.Writer.Header().Add("Content-Disposition",
		fmt.Sprintf("attachment;filename=%s", filepath.Base(fileName)))
	context.File(fileName) //返回文件路径，自动调用http.ServeFile方法

	return
}

func ApiImportDeviceTSLPlugin(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	// 获取物模型名称
	TSLName := context.PostForm("TSLName")

	index := -1
	for k, v := range device.DeviceTSLMap {
		if v.Name == TSLName {
			index = k
		}
	}
	if index == -1 {
		setting.ZAPS.Errorf("TSLName is not exist")
		aParam.Code = "1"
		aParam.Message = "TSLName is not exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	// 获取文件头
	file, err := context.FormFile("FileName")
	if err != nil {
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	fileName := "./plugin/" + file.Filename
	fileDir := "./plugin/" + TSLName

	utils.DirIsExist("./plugin/" + TSLName)

	//保存文件到服务器本地
	if err := context.SaveUploadedFile(file, fileName); err != nil {
		aParam.Code = "1"
		aParam.Message = "save File Error"

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	defer os.Remove(fileName)

	err = unZip(fileName, fileDir)
	if err != nil {
		setting.ZAPS.Errorf("unZip err %s", fileName)
	}
	err = os.Remove(fileName)
	if err != nil {
		setting.ZAPS.Errorf("removeFile err %s", fileName)
	}

	fileNameWithSuffix := path.Base(fileName)
	fileType := path.Ext(fileName)
	plugin := strings.TrimSuffix(fileNameWithSuffix, fileType)
	device.DeviceTSLModifyPlugin(TSLName, plugin)

	aParam.Code = "0"
	aParam.Message = ""
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiExportDeviceTSLPlugin(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	tslName := context.Query("TSLName")
	index := -1
	for k, v := range device.DeviceTSLMap {
		if v.Name == tslName {
			index = k
		}
	}

	if index == -1 {
		aParam.Code = "1"
		aParam.Message = "tslName is not Exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	status, name := device.DeviceTSLExportPlugin(device.DeviceTSLMap[index].Plugin)
	if status == true {
		//返回文件流
		context.Writer.Header().Add("Content-Disposition",
			fmt.Sprintf("attachment;filename=%s", filepath.Base(name)))
		context.File(name) //返回文件路径，自动调用http.ServeFile方法

	} else {
		context.JSON(http.StatusOK, model.ResponseData{
			Code:    "1",
			Message: "",
			Data:    "",
		})
	}
}

func ApiAddDeviceTSLProperty(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	tslInfo := &struct {
		TSLName  string                           `json:"TSLName"`  // 名称
		Property device.DeviceTSLPropertyTemplate `json:"Property"` //
	}{}

	err := json.Unmarshal(bodyBuf[:n], tslInfo)
	if err != nil {
		setting.ZAPS.Errorf("AddDeviceTSLProperty json unMarshall err %v", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}
	setting.ZAPS.Debugf("AddDeviceTSLProperty %v", tslInfo)

	index := -1
	for k, v := range device.DeviceTSLMap {
		if v.Name == tslInfo.TSLName {
			index = k
		}
	}
	if index == -1 {
		setting.ZAPS.Errorf("DeviceTSLAdd err %v", err)
		aParam.Code = "1"
		aParam.Message = "TSLName is not exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	_, err = device.DeviceTSLMap[index].DeviceTSLPropertiesAdd(tslInfo.Property)

	eventMsg := device.DeviceTSLEventTemplate{
		Type:  device.DeviceTSLMap[index].Name,
		Topic: "modify",
	}
	device.DeviceTSLMap[index].Event.Publish("modify", eventMsg)

	aParam.Code = "0"
	aParam.Message = ""
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiModifyDeviceTSLProperty(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	tslInfo := &struct {
		TSLName  string                           `json:"TSLName"`  // 名称
		Property device.DeviceTSLPropertyTemplate `json:"Property"` //
	}{}

	err := json.Unmarshal(bodyBuf[:n], tslInfo)
	if err != nil {
		setting.ZAPS.Errorf("ModifyDeviceTSLProperty json unMarshall err %v", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}
	setting.ZAPS.Debugf("ModifyDeviceTSLProperty %v", tslInfo)

	index := -1
	for k, v := range device.DeviceTSLMap {
		if v.Name == tslInfo.TSLName {
			index = k
		}
	}
	if index == -1 {
		setting.ZAPS.Errorf("DeviceTSLModify err %v", err)
		aParam.Code = "1"
		aParam.Message = "TSLName is not exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	_, err = device.DeviceTSLMap[index].DeviceTSLPropertiesModify(&tslInfo.Property)
	eventMsg := device.DeviceTSLEventTemplate{
		Type:  device.DeviceTSLMap[index].Name,
		Topic: "modify",
	}
	device.DeviceTSLMap[index].Event.Publish("modify", eventMsg)

	aParam.Code = "0"
	aParam.Message = ""
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiDeleteDeviceTSLProperties(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	type PropertyNameTemplate struct {
		Name string
	}

	tslInfo := &struct {
		TSLName    string                 `json:"TSLName"`    // 名称
		Properties []PropertyNameTemplate `json:"Properties"` //
	}{}

	err := json.Unmarshal(bodyBuf[:n], tslInfo)
	if err != nil {
		setting.ZAPS.Errorf("DeleteDeviceTSLProperties json unMarshall err %v", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}
	setting.ZAPS.Debugf("DeleteDeviceTSLProperties %v", tslInfo)

	index := -1
	for k, v := range device.DeviceTSLMap {
		if v.Name == tslInfo.TSLName {
			index = k
		}
	}
	if index == -1 {
		setting.ZAPS.Errorf("DeviceTSLDelete err %v", err)
		aParam.Code = "1"
		aParam.Message = "TSLName is not exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	propertyNames := make([]string, 0)
	for _, v := range tslInfo.Properties {
		propertyNames = append(propertyNames, v.Name)
	}
	_, err = device.DeviceTSLMap[index].DeviceTSLPropertiesDelete(propertyNames)
	if err != nil {
		aParam.Code = "1"
		aParam.Message = err.Error()
	} else {
		eventMsg := device.DeviceTSLEventTemplate{
			Type:  device.DeviceTSLMap[index].Name,
			Topic: "modify",
		}
		device.DeviceTSLMap[index].Event.Publish("modify", eventMsg)

		aParam.Code = "0"
		aParam.Message = ""
		device.WriteDeviceTSLParamToJson()
	}
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiGetDeviceTSLProperties(context *gin.Context) {

	aParam := struct {
		Code    string                             `json:"Code"`
		Message string                             `json:"Message"`
		Data    []device.DeviceTSLPropertyTemplate `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    make([]device.DeviceTSLPropertyTemplate, 0),
	}

	tslName := context.Query("TSLName")
	for _, v := range device.DeviceTSLMap {
		if v.Name == tslName {
			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = append(aParam.Data, v.Properties...)

			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}

	}

	aParam.Code = "1"
	aParam.Message = "tslName is not exist"
	aParam.Data = make([]device.DeviceTSLPropertyTemplate, 0)
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiAddDeviceTSLService(context *gin.Context) {
	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	tslInfo := &struct {
		TSLName string                          `json:"TSLName"` // 名称
		Service device.DeviceTSLServiceTempalte `json:"Service"` //
	}{}

	err := json.Unmarshal(bodyBuf[:n], tslInfo)
	if err != nil {
		setting.ZAPS.Errorf("AddDeviceTSLService json unMarshall err %v", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}
	setting.ZAPS.Debugf("AddDeviceTSLService %v", tslInfo)

	index := -1
	for k, v := range device.DeviceTSLMap {
		if v.Name == tslInfo.TSLName {
			index = k
		}
	}
	if index == -1 {
		setting.ZAPS.Errorf("DeviceTSLAddService err %v", err)
		aParam.Code = "1"
		aParam.Message = "TSLName is not exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	_, err = device.DeviceTSLMap[index].DeviceTSLServicesAdd(tslInfo.Service)

	aParam.Code = "0"
	aParam.Message = ""
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiModifyDeviceTSLService(context *gin.Context) {
	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	tslInfo := &struct {
		TSLName string                          `json:"TSLName"` // 名称
		Service device.DeviceTSLServiceTempalte `json:"Service"` //
	}{}

	err := json.Unmarshal(bodyBuf[:n], tslInfo)
	if err != nil {
		setting.ZAPS.Errorf("ModifyDeviceTSLService json unMarshall err %v", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}
	setting.ZAPS.Debugf("ModifyDeviceTSLService %v", tslInfo)

	index := -1
	for k, v := range device.DeviceTSLMap {
		if v.Name == tslInfo.TSLName {
			index = k
		}
	}
	if index == -1 {
		setting.ZAPS.Errorf("DeviceTSLModifyService err %v", err)
		aParam.Code = "1"
		aParam.Message = "TSLName is not exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	_, err = device.DeviceTSLMap[index].DeviceTSLServicesModify(&tslInfo.Service)

	aParam.Code = "0"
	aParam.Message = ""
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiDeleteDeviceTSLServices(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	fmt.Println(string(bodyBuf[:n]))

	type ServiceTemplate struct {
		Name string
	}

	tslInfo := &struct {
		TSLName  string            `json:"TSLName"`  // 名称
		Services []ServiceTemplate `json:"Services"` //
	}{}

	err := json.Unmarshal(bodyBuf[:n], tslInfo)
	if err != nil {
		setting.ZAPS.Errorf("DeleteDeviceTSLServices json unMarshall err %v", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"

		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}
	setting.ZAPS.Debugf("DeleteDeviceTSLServices %v", tslInfo)

	index := -1
	for k, v := range device.DeviceTSLMap {
		if v.Name == tslInfo.TSLName {
			index = k
		}
	}
	if index == -1 {
		setting.ZAPS.Errorf("DeviceTSLDelete err %v", err)
		aParam.Code = "1"
		aParam.Message = "TSLName is not exist"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	serviceNames := make([]string, 0)
	for _, v := range tslInfo.Services {
		serviceNames = append(serviceNames, v.Name)
	}
	_, err = device.DeviceTSLMap[index].DeviceTSLServicesDelete(serviceNames)
	if err != nil {
		aParam.Code = "1"
		aParam.Message = err.Error()
	} else {
		aParam.Code = "0"
		aParam.Message = ""
		device.WriteDeviceTSLParamToJson()
	}
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}
