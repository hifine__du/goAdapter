package mqttHuawei

import (
	"encoding/json"
	"goAdapter/device"
	"goAdapter/setting"
)

func MQTTHuaweiGetPropertiesAck(r *ReportServiceParamHuaweiTemplate, service MQTTHuaweiServiceTemplate) {

	type MQTTHuaweiDeviceServiceTemplate struct {
		Services []MQTTHuaweiServiceTemplate `json:"services"`
	}

	deviceService := MQTTHuaweiDeviceServiceTemplate{
		Services: make([]MQTTHuaweiServiceTemplate, 0),
	}

	deviceService.Services = append(deviceService.Services, service)

	sJson, _ := json.Marshal(deviceService)
	setting.ZAPS.Debugf("thingServiceAck post msg: %s", sJson)

	serviceTopic := "$oc/devices/" + r.GWParam.Param.DeviceID + "/sys/properties/get/response/" + service.ServiceID
	setting.ZAPS.Infof("thingServiceAck post topic: %s", serviceTopic)

	if r.GWParam.MQTTClient != nil {
		token := r.GWParam.MQTTClient.Publish(serviceTopic, 0, false, sJson)
		token.Wait()
	}

}

func ReportServiceHuaweiProcessGetProperties(r *ReportServiceParamHuaweiTemplate, request MQTTHuaweiGetPropertiesRequestTemplate) {

	x := -1
	for k, v := range r.NodeList {
		if v.Param.DeviceID == request.ObjectDeviceID {
			x = k
			break
		}
	}
	if x == -1 {
		return
	}

	coll, ok := device.CollectInterfaceMap.Coll[r.NodeList[x].CollInterfaceName]
	if !ok {
		return
	}

	node, ok := coll.DeviceNodeMap[r.NodeList[x].Name]
	if !ok {
		return
	}

	cmd := device.CommunicationCmdTemplate{}
	cmd.CollInterfaceName = coll.CollInterfaceName
	cmd.DeviceName = node.Name
	cmd.FunName = "GetRealVariables"
	cmd.FunPara = ""

	cmdRX := coll.CommQueueManage.CommunicationManageAddEmergency(cmd)
	if cmdRX.Status == true {
		setting.ZAPS.Debugf("GetRealVariables ok")
		service := MQTTHuaweiServiceTemplate{}
		for _, v := range node.Properties {
			if v.Name == request.ServiceID {
				if len(v.Value) >= 1 {
					index := len(v.Value) - 1
					service := MQTTHuaweiServiceTemplate{}
					service.ServiceID = v.Name
					service.Properties.Value = v.Value[index].Value
				}
			}
		}
		MQTTHuaweiGetPropertiesAck(r, service)
	}
}
