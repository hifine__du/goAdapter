package mqttEmqx

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"goAdapter/device"
	"goAdapter/device/eventBus"
	"goAdapter/setting"
	"goAdapter/utils"
	"math"
	"os"
	"strconv"
	"strings"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/robfig/cron"
)

//上报节点参数结构体
type ReportServiceNodeParamEmqxTemplate struct {
	ServiceName       string
	CollInterfaceName string
	Name              string
	Addr              string
	CommStatus        string
	ReportErrCnt      int `json:"-"`
	ReportStatus      string
	Protocol          string
	Param             struct {
		ClientID string
	}
}

//上报网关参数结构体
type ReportServiceGWParamEmqxTemplate struct {
	ServiceName  string
	IP           string
	Port         string
	ReportStatus string
	ReportTime   int
	ReportErrCnt int
	Protocol     string
	Param        struct {
		UserName     string
		Password     string
		ClientID     string
		KeepAlive    string
		CleanSession bool
	}
	MQTTClient   MQTT.Client `json:"-"`
	MQTTClientID int         `json:"-"`
	OfflineChan  chan bool   `json:"-"`
}

//上报服务参数，网关参数，节点参数
type ReportServiceParamEmqxTemplate struct {
	GWParam                               ReportServiceGWParamEmqxTemplate
	NodeList                              []ReportServiceNodeParamEmqxTemplate
	ReceiveFrameChan                      chan MQTTEmqxReceiveFrameTemplate          `json:"-"`
	LogInRequestFrameChan                 chan []string                              `json:"-"` //上线
	ReceiveLogInAckFrameChan              chan MQTTEmqxLogInAckTemplate              `json:"-"`
	LogOutRequestFrameChan                chan []string                              `json:"-"`
	ReceiveLogOutAckFrameChan             chan MQTTEmqxLogOutAckTemplate             `json:"-"`
	ReportPropertyRequestFrameChan        chan MQTTEmqxReportPropertyTemplate        `json:"-"`
	ReceiveReportPropertyAckFrameChan     chan MQTTEmqxReportPropertyAckTemplate     `json:"-"`
	ReceiveInvokeServiceRequestFrameChan  chan MQTTEmqxInvokeServiceRequestTemplate  `json:"-"`
	ReceiveInvokeServiceAckFrameChan      chan MQTTEmqxInvokeServiceAckTemplate      `json:"-"`
	ReceiveWritePropertyRequestFrameChan  chan MQTTEmqxWritePropertyRequestTemplate  `json:"-"`
	ReceiveReadPropertyRequestFrameChan   chan MQTTEmqxReadPropertyRequestTemplate   `json:"-"`
	ReceiveReadNodeStatusRequestFrameChan chan MQTTEmqxReadNodeStatusRequestTemplate `json:"-"`
	CancelFunc                            context.CancelFunc                         `json:"-"`
}

type ReportServiceParamListEmqxTemplate struct {
	ServiceList []*ReportServiceParamEmqxTemplate
}

//实例化上报服务
var ReportServiceParamListEmqx ReportServiceParamListEmqxTemplate

func ReportServiceEmqxReadParamFromJson() bool {
	type ReportServiceConfigParamEmqxTemplate struct {
		ServiceList []ReportServiceParamEmqxTemplate `json:"ServiceList"`
	}

	configParam := ReportServiceConfigParamEmqxTemplate{}
	data, err := utils.FileRead("/selfpara/reportServiceParamListEmqx.json")
	if err != nil {
		setting.ZAPS.Debugf("上报服务[Emqx]配置json文件读取失败 %v", err)
		return false
	}
	err = json.Unmarshal(data, &configParam)
	if err != nil {
		setting.ZAPS.Errorf("上报服务[Emqx]配置json文件格式化失败")
		return false
	}
	setting.ZAPS.Debug("上报服务[Emqx]配置json文件读取成功")

	//初始化
	for _, v := range configParam.ServiceList {
		ReportServiceParamListEmqx.ServiceList = append(ReportServiceParamListEmqx.ServiceList, NewReportServiceParamEmqx(v.GWParam, v.NodeList))
	}

	return true
}

func ReportServiceEmqxWriteParamToJson() {
	utils.DirIsExist("./selfpara")
	sJson, _ := json.Marshal(ReportServiceParamListEmqx)
	err := utils.FileWrite("/selfpara/reportServiceParamListEmqx.json", sJson)
	if err != nil {
		setting.ZAPS.Errorf("上报服务[Emqx]配置json文件写入失败")
		return
	}
	setting.ZAPS.Debugf("上报服务[Emqx]配置json文件写入成功")
}

func (s *ReportServiceParamListEmqxTemplate) AddReportService(param ReportServiceGWParamEmqxTemplate) {

	for k, v := range s.ServiceList {
		//存在相同的，表示修改;不存在表示增加
		if v.GWParam.ServiceName == param.ServiceName {
			s.ServiceList[k].GWParam = param
			ReportServiceEmqxWriteParamToJson()
			return
		}
	}

	nodeList := make([]ReportServiceNodeParamEmqxTemplate, 0)
	ReportServiceParam := NewReportServiceParamEmqx(param, nodeList)
	s.ServiceList = append(s.ServiceList, ReportServiceParam)

	ReportServiceEmqxWriteParamToJson()
}

func (s *ReportServiceParamListEmqxTemplate) DeleteReportService(serviceName string) {

	for k, v := range s.ServiceList {
		if v.GWParam.ServiceName == serviceName {
			s.ServiceList = append(s.ServiceList[:k], s.ServiceList[k+1:]...)
			ReportServiceEmqxWriteParamToJson()
			//协程退出
			v.CancelFunc()
			return
		}
	}
}

func (r *ReportServiceParamEmqxTemplate) AddReportNode(param ReportServiceNodeParamEmqxTemplate) {

	param.CommStatus = "offLine"
	param.ReportStatus = "offLine"
	param.ReportErrCnt = 0

	//节点存在则进行修改
	for k, v := range r.NodeList {
		//节点已经存在
		if v.Name == param.Name {
			r.NodeList[k] = param
			ReportServiceEmqxWriteParamToJson()
			return
		}
	}

	//节点不存在则新建
	r.NodeList = append(r.NodeList, param)
	ReportServiceEmqxWriteParamToJson()

	setting.ZAPS.Debugf("param %v", ReportServiceParamListEmqx)
}

func (r *ReportServiceParamEmqxTemplate) DeleteReportNode(name string) int {

	index := -1
	//节点存在则进行修改
	for k, v := range r.NodeList {
		//节点已经存在
		if v.Name == name {
			index = k
			r.NodeList = append(r.NodeList[:k], r.NodeList[k+1:]...)
			ReportServiceEmqxWriteParamToJson()
			return index
		}
	}
	return index
}

func (r *ReportServiceParamEmqxTemplate) ExportParamToCsv() (bool, string) {

	//创建文件
	utils.DirIsExist("./tmp")
	fileName := "./tmp/" + r.GWParam.ServiceName + ".csv"

	fs, err := os.Create(fileName)
	if err != nil {
		setting.ZAPS.Errorf("创建csv文件错误 %v", err)
		return false, ""
	}
	defer fs.Close()

	//创建一个新的写入文件流
	w := csv.NewWriter(fs)
	csvData := [][]string{
		{"上报服务名称", "采集接口名称", "设备名称", "设备通信地址", "上报服务协议", "客户端ID"},
		{"ServiceName", "CollInterfaceName", "Name", "Addr", "Protocol", "ClientID"},
	}

	for _, n := range r.NodeList {
		param := make([]string, 0)
		param = append(param, n.ServiceName)
		param = append(param, n.CollInterfaceName)
		param = append(param, n.Name)
		param = append(param, n.Addr)
		param = append(param, n.Protocol)
		param = append(param, n.Param.ClientID)
		csvData = append(csvData, param)
	}

	//写入数据
	err = w.WriteAll(csvData)
	if err != nil {
		setting.ZAPS.Errorf("写csv文件错误 %v", err)
		return false, ""
	}
	w.Flush()

	return true, fileName
}

func (r *ReportServiceParamEmqxTemplate) ProcessUpLinkFrame(ctx context.Context) {

	for {
		select {
		case <-ctx.Done():
			{
				return
			}
		case reqFrame := <-r.LogInRequestFrameChan:
			{
				r.LogIn(reqFrame)
			}
		case reqFrame := <-r.LogOutRequestFrameChan:
			{
				r.LogOut(reqFrame)
			}
		case reqFrame := <-r.ReportPropertyRequestFrameChan:
			{
				if reqFrame.DeviceType == "gw" {
					r.GWPropertyPost()
				} else if reqFrame.DeviceType == "node" {
					r.NodePropertyPost(reqFrame.DeviceName)
				}
			}
		case reqFrame := <-r.ReceiveWritePropertyRequestFrameChan:
			{
				r.ReportServiceEmqxProcessWriteProperty(reqFrame)
			}
		case reqFrame := <-r.ReceiveReadPropertyRequestFrameChan:
			{
				r.ReportServiceEmqxProcessReadProperty(reqFrame)
			}
		case reqFrame := <-r.ReceiveInvokeServiceRequestFrameChan:
			{
				r.ReportServiceEmqxProcessInvokeService(reqFrame)
			}
		case reqFrame := <-r.ReceiveReadNodeStatusRequestFrameChan:
			{
				r.ReportServiceEmqxProcessReadNodeStatus(reqFrame)
			}
		}
	}
}

func (r *ReportServiceParamEmqxTemplate) ProcessDownLinkFrame(ctx context.Context) {

	for {
		select {
		case <-ctx.Done():
			{
				return
			}
		case frame := <-r.ReceiveFrameChan:
			{
				//setting.ZAPS.Debugf("Recv TOPIC: %s", frame.Topic)
				//setting.ZAPS.Debugf("Recv MSG: %s", frame.Payload)
				if strings.Contains(frame.Topic, "/sys/thing/event/property/post_reply") { //网关、子设备上报属性回应

					ackFrame := MQTTEmqxReportPropertyAckTemplate{}
					err := json.Unmarshal(frame.Payload, &ackFrame)
					if err != nil {
						setting.ZAPS.Errorf("ReportPropertyAck json unmarshal err")
						continue
					}
					r.ReceiveReportPropertyAckFrameChan <- ackFrame
				} else if strings.Contains(frame.Topic, "/sys/thing/event/login/post_reply") { //子设备上线回应

					ackFrame := MQTTEmqxLogInAckTemplate{}
					err := json.Unmarshal(frame.Payload, &ackFrame)
					if err != nil {
						setting.ZAPS.Warnf("LogInAck json unmarshal err")
						continue
					}
					r.ReceiveLogInAckFrameChan <- ackFrame
				} else if strings.Contains(frame.Topic, "/sys/thing/event/logout/post_reply") { //子设备下线回应

					ackFrame := MQTTEmqxLogOutAckTemplate{}
					err := json.Unmarshal(frame.Payload, &ackFrame)
					if err != nil {
						setting.ZAPS.Errorf("LogOutAck json unmarshal err")
						continue
					}
					r.ReceiveLogOutAckFrameChan <- ackFrame
				} else if strings.Contains(frame.Topic, "/sys/thing/event/service/invoke") { //设备服务调用
					serviceFrame := MQTTEmqxInvokeServiceRequestTemplate{}
					err := json.Unmarshal(frame.Payload, &serviceFrame)
					if err != nil {
						setting.ZAPS.Errorf("serviceFrame json unmarshal err")
						continue
					}
					r.ReceiveInvokeServiceRequestFrameChan <- serviceFrame
				} else if strings.Contains(frame.Topic, "/sys/thing/event/property/set") { //设置属性请求
					writePropertyFrame := MQTTEmqxWritePropertyRequestTemplate{}
					err := json.Unmarshal(frame.Payload, &writePropertyFrame)
					if err != nil {
						setting.ZAPS.Errorf("writePropertyFrame json unmarshal err")
						continue
					}
					r.ReceiveWritePropertyRequestFrameChan <- writePropertyFrame
				} else if strings.Contains(frame.Topic, "/sys/thing/event/property/get") { //获取属性请求
					readPropertyFrame := MQTTEmqxReadPropertyRequestTemplate{}
					err := json.Unmarshal(frame.Payload, &readPropertyFrame)
					if err != nil {
						setting.ZAPS.Errorf("readPropertyFrame json unmarshal err")
						continue
					}
					r.ReceiveReadPropertyRequestFrameChan <- readPropertyFrame
				} else if strings.Contains(frame.Topic, "/sys/thing/event/node/status/get") { //获取设备状态
					readNodeStatusFrame := MQTTEmqxReadNodeStatusRequestTemplate{}
					err := json.Unmarshal(frame.Payload, &readNodeStatusFrame)
					if err != nil {
						setting.ZAPS.Errorf("readNodeStatusFrame json unmarshal err")
						continue
					}
					r.ReceiveReadNodeStatusRequestFrameChan <- readNodeStatusFrame
				}
			}
		}
	}
}

func (r *ReportServiceParamEmqxTemplate) ProcessCollEvent(ctx context.Context, sub eventBus.Sub) {
	for {
		select {
		case <-ctx.Done():
			{
				return
			}
		case msg := <-sub.Out():
			{
				subMsg := msg.(device.CollectInterfaceEventTemplate)
				//判断设备在该上报服务中
				index := -1
				for k, v := range r.NodeList {
					if v.Name == subMsg.NodeName {
						index = k
					}
				}
				if index == -1 {
					continue
				}
				setting.ZAPS.Debugf("上报服务[%s] 采集接口[%s] 设备[%s] 主题[%s] 消息内容[%v]",
					r.GWParam.ServiceName,
					subMsg.CollName,
					subMsg.NodeName,
					subMsg.Topic,
					subMsg.Content)
				nodeName := make([]string, 0)
				switch subMsg.Topic {
				case "onLine":
					{
						nodeName = append(nodeName, subMsg.NodeName)
						r.NodeList[index].CommStatus = "onLine"
						r.LogInRequestFrameChan <- nodeName
					}
				case "offLine":
					{
						nodeName = append(nodeName, subMsg.NodeName)
						r.NodeList[index].CommStatus = "offLine"
						r.LogOutRequestFrameChan <- nodeName
					}
				case "update":
					{
						//更新设备的通信状态
						r.NodeList[index].CommStatus = "onLine"

						coll, ok := device.CollectInterfaceMap.Coll[subMsg.CollName]
						if !ok {
							continue
						}
						node, ok := coll.DeviceNodeMap[subMsg.NodeName]
						if !ok {
							return
						}

						reportStatus := false
						for _, v := range node.Properties {
							if v.Params.StepAlarm == true {
								valueCnt := len(v.Value)
								if valueCnt >= 2 { //阶跃报警必须是2个值
									if v.Type == device.PropertyTypeInt32 {
										pValueCur := v.Value[valueCnt-1].Value.(int32)
										pValuePre := v.Value[valueCnt-2].Value.(int32)
										step, _ := strconv.Atoi(v.Params.Step)
										if math.Abs(float64(pValueCur-pValuePre)) > float64(step) {
											reportStatus = true //满足报警条件，上报
											nodeName = append(nodeName, node.Name)
										}
									} else if v.Type == device.PropertyTypeUInt32 {
										pValueCur := v.Value[valueCnt-1].Value.(uint32)
										pValuePre := v.Value[valueCnt-2].Value.(uint32)
										step, _ := strconv.Atoi(v.Params.Step)
										if math.Abs(float64(pValueCur-pValuePre)) > float64(step) {
											reportStatus = true //满足报警条件，上报
											nodeName = append(nodeName, node.Name)
										}
									} else if v.Type == device.PropertyTypeDouble {
										pValueCur := v.Value[valueCnt-1].Value.(float64)
										pValuePre := v.Value[valueCnt-2].Value.(float64)
										step, err := strconv.ParseFloat(v.Params.Step, 64)
										if err != nil {
											continue
										}
										if math.Abs(pValueCur-pValuePre) > float64(step) {
											reportStatus = true //满足报警条件，上报
											nodeName = append(nodeName, node.Name)
										}
									}
								}
							} else if v.Params.MinMaxAlarm == true {
								valueCnt := len(v.Value)
								if v.Type == device.PropertyTypeInt32 {
									pValueCur := v.Value[valueCnt-1].Value.(int32)
									min, _ := strconv.Atoi(v.Params.Min)
									max, _ := strconv.Atoi(v.Params.Max)
									if pValueCur < int32(min) || pValueCur > int32(max) {
										reportStatus = true //满足报警条件，上报
										nodeName = append(nodeName, node.Name)
									}
								} else if v.Type == device.PropertyTypeUInt32 {
									pValueCur := v.Value[valueCnt-1].Value.(uint32)
									min, _ := strconv.Atoi(v.Params.Min)
									max, _ := strconv.Atoi(v.Params.Max)
									if pValueCur < uint32(min) || pValueCur > uint32(max) {
										reportStatus = true //满足报警条件，上报
										nodeName = append(nodeName, node.Name)
									}
								} else if v.Type == device.PropertyTypeDouble {
									pValueCur := v.Value[valueCnt-1].Value.(float64)
									min, err := strconv.ParseFloat(v.Params.Min, 64)
									if err != nil {
										continue
									}
									max, err := strconv.ParseFloat(v.Params.Max, 64)
									if err != nil {
										continue
									}
									if pValueCur < min || pValueCur > max {
										reportStatus = true //满足报警条件，上报
										nodeName = append(nodeName, node.Name)
									}
								}
							}
						}

						if reportStatus == true {
							reportNodeProperty := MQTTEmqxReportPropertyTemplate{
								DeviceType: "node",
								DeviceName: nodeName,
							}
							r.ReportPropertyRequestFrameChan <- reportNodeProperty
						}
					}
				}
			}
		}
	}
}

func (r *ReportServiceParamEmqxTemplate) LogIn(nodeName []string) {

	//清空接收chan，避免出现有上次接收的缓存
	for i := 0; i < len(r.ReceiveLogInAckFrameChan); i++ {
		<-r.ReceiveLogInAckFrameChan
	}

	r.NodeLogIn(nodeName)
}

func (r *ReportServiceParamEmqxTemplate) LogOut(nodeName []string) {

	//清空接收chan，避免出现有上次接收的缓存
	for i := 0; i < len(r.ReceiveLogOutAckFrameChan); i++ {
		<-r.ReceiveLogOutAckFrameChan
	}

	r.NodeLogOut(nodeName)
}

func (r *ReportServiceParamEmqxTemplate) ReportTimeFun() {

	//网关上报
	reportGWProperty := MQTTEmqxReportPropertyTemplate{
		DeviceType: "gw",
	}
	r.ReportPropertyRequestFrameChan <- reportGWProperty
	if r.GWParam.ReportStatus == "onLine" {
		//全部末端设备上报
		nodeName := make([]string, 0)
		for _, v := range r.NodeList {
			if v.CommStatus == "onLine" {
				nodeName = append(nodeName, v.Name)
			}
		}
		setting.ZAPS.Debugf("上报服务[%s]定时上报任务中上报节点%v", r.GWParam.ServiceName, nodeName)
		if len(nodeName) > 0 {
			reportNodeProperty := MQTTEmqxReportPropertyTemplate{
				DeviceType: "node",
				DeviceName: nodeName,
			}
			r.ReportPropertyRequestFrameChan <- reportNodeProperty
		}
	}
}

func ReportServiceEmqxInit() {
	ReportServiceEmqxReadParamFromJson()
}

func NewReportServiceParamEmqx(gw ReportServiceGWParamEmqxTemplate, nodeList []ReportServiceNodeParamEmqxTemplate) *ReportServiceParamEmqxTemplate {
	emqxParam := &ReportServiceParamEmqxTemplate{
		GWParam:                               gw,
		NodeList:                              nodeList,
		ReceiveFrameChan:                      make(chan MQTTEmqxReceiveFrameTemplate, 100),
		LogInRequestFrameChan:                 make(chan []string, 0),
		ReceiveLogInAckFrameChan:              make(chan MQTTEmqxLogInAckTemplate, 5),
		LogOutRequestFrameChan:                make(chan []string, 0),
		ReceiveLogOutAckFrameChan:             make(chan MQTTEmqxLogOutAckTemplate, 5),
		ReportPropertyRequestFrameChan:        make(chan MQTTEmqxReportPropertyTemplate, 50),
		ReceiveReportPropertyAckFrameChan:     make(chan MQTTEmqxReportPropertyAckTemplate, 50),
		ReceiveInvokeServiceRequestFrameChan:  make(chan MQTTEmqxInvokeServiceRequestTemplate, 50),
		ReceiveInvokeServiceAckFrameChan:      make(chan MQTTEmqxInvokeServiceAckTemplate, 50),
		ReceiveWritePropertyRequestFrameChan:  make(chan MQTTEmqxWritePropertyRequestTemplate, 50),
		ReceiveReadPropertyRequestFrameChan:   make(chan MQTTEmqxReadPropertyRequestTemplate, 50),
		ReceiveReadNodeStatusRequestFrameChan: make(chan MQTTEmqxReadNodeStatusRequestTemplate, 50),
	}

	ctx, cancel := context.WithCancel(context.Background())
	emqxParam.CancelFunc = cancel

	go ReportServiceEmqxPoll(ctx, emqxParam)

	return emqxParam
}

func ReportServiceEmqxPoll(ctx context.Context, r *ReportServiceParamEmqxTemplate) {

	reportState := 0

	// 定义一个cron运行器
	cronProcess := cron.New()

	reportTime := fmt.Sprintf("@every %dm%ds", r.GWParam.ReportTime/60, r.GWParam.ReportTime%60)
	setting.ZAPS.Infof("上报服务[%s]定时上报周期为%v", r.GWParam.ServiceName, reportTime)

	_ = cronProcess.AddFunc(reportTime, r.ReportTimeFun)

	//订阅采集接口消息
	device.CollectInterfaceMap.Lock.Lock()
	for _, coll := range device.CollectInterfaceMap.Coll {
		sub := eventBus.NewSub()
		coll.CollEventBus.Subscribe("onLine", sub)
		coll.CollEventBus.Subscribe("offLine", sub)
		coll.CollEventBus.Subscribe("update", sub)
		go r.ProcessCollEvent(ctx, sub)
	}
	device.CollectInterfaceMap.Lock.Unlock()

	go r.ProcessUpLinkFrame(ctx)
	go r.ProcessDownLinkFrame(ctx)

	for {
		select {
		case <-ctx.Done():
			{
				return
			}
		default:
			{
				if reportState == 0 {
					if r.GWLogin() == true {
						reportState = 1
						cronProcess.Start()
					} else {
						time.Sleep(5 * time.Second)
					}
				}
				time.Sleep(100 * time.Millisecond)
			}
		}
	}
}
