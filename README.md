#### 技术交流

QQ群1028704210

#### 官网及驱动下载

http://www.opengw.cn

http://www.opengw.cn/col.jsp?id=104

#### 可执行文件下载

https://gitee.com/my_iot/goAdapter/releases

#### 视频教程

https://www.bilibili.com/video/BV1wg41157m9?from=search&seid=7860625376747338929&spm_id_from=333.337.0.0



#### 背景

#### 框架设计

<div align=center><img src="https://images.gitee.com/uploads/images/2021/0330/152140_b19a7690_1979498.png"/></div>

软件主要分成3层：

- 应用接口

> 用于与上层应用系统进行通信，可以设置定时上报硬件设备数据到物联网平台，或者接收物联网平台下发命令，转发给硬件设备；采用Json等格式数据与上层应用系统通信，对接更简单；

- 采集接口

> - 用于对硬件设备进行管理，支持对设备数量、设备类型、设备属性的增、删、查、改等操作，同时可以设置定时采集设备的属性并缓存，方便上层应用系统对硬件设备操作；
> - 支持采用Lua脚本实现对设备通信协议的编写，方便灵活；

- 通信接口

> 对物理通信接口的封装，比如串口、网络、GPIO等，封装接口后对上提供读取和写入2个接口，方便上层调用；

#### 功能特点

- 采用golang语言设计，运行效率高，跨平台方便；
- 内置WebServer，网页配置更方便、更快捷
- 采用Lua脚本，增加设备类型时不需要重新编码后台代码，更方便灵活；
- 支持MqttClient，ModbusTCPServer，OPCUaServer等通信，采用JSON格式通信，上层系统对接更快捷；
- 支持CSV文件导入功能，批量添加；
- 支持配置文件的备份和回复；

#### 可执行文件运行

<img src="./doc/img/可执行文件说明1.png" alt="可执行文件说明1" style="zoom:50%;" />

下载后解压如下图所示：

<img src="./doc/img/可执行文件压缩包解压.png" alt="可执行文件压缩包解压" style="zoom:50%;" />

将所有文件拷贝到硬件中（如果是电脑的，直接运行即可）

- windows：双击openGW_xx.exe
- linux系统：./openGW_xx &

然后在浏览器中输入127.0.0.1:8080，注意加上端口，即可正常访问页面

#### 编译运行

1、golang环境搭建及goland安装 大家可以自行百度

2、goland运行

右上角选择编译配置

<img src="./doc/img/编译设置1.png" alt="编译设置1" style="zoom:50%;" />



配置-》运行种类-》选择目录，输出目录选择当前文件夹，即输出文件和webroot等文件在同一级目录

<img src="./doc/img/编译配置2.png" alt="编译配置2" style="zoom:50%;" />


然后在浏览器中输入127.0.0.1:8080，注意加上端口，即可正常访问页面

#### 功能介绍

1. 通信接口
2. 采集接口
3. 应用接口

